;(package-initialize) ; For now, required by flx-ido.

;; el-get setup.
(add-to-list 'load-path "~/.emacs.d/el-get/el-get")

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.githubusercontent.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

(add-to-list 'el-get-recipe-path "~/.emacs.d/el-get-user/recipes")

(el-get 'sync)
;; Packages.
(el-get-bundle base16 :shallow t)
(el-get-bundle evil :shallow t)
;(el-get-bundle flx-ido)
(el-get-bundle magit :shallow t)
(el-get-bundle projectile :shallow t)
(el-get-bundle undo-tree :shallow t)
(el-get-bundle yasnippet :shallow t)
(el-get-bundle expand-region :shallow t)
(el-get-bundle powerline :shallow t)
(el-get-bundle web-mode :shallow t)
(el-get-bundle emmet-mode :shallow t)
(el-get-bundle go-mode :shallow t)
(el-get-bundle rust-mode :shallow t)
(el-get-bundle auto-complete :shallow t)
(el-get-bundle mu4e :shallow t)
(el-get-bundle helm :shallow t)
(el-get-bundle helm-projectile :shallow t)
(el-get-bundle slime :shallow t)
(el-get-bundle flycheck :shallow t)

;(setq package-selected-packages '(flx-ido))

(require 'linum)
(require 'yasnippet)

(load-theme 'base16-default-light t)

;; TODO: along with linum styling, make a hook for load-theme to set these
;; faces. This is because I change the theme sometimes and they override these.
(set-face-background 'fringe (face-background 'default))

(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

(setq inhibit-splash-screen t)
(setq initial-scratch-message ";; Have fun with Lisp!\n")
(setq c-default-style "bsd")
(setq whitespace-style '(face trailing lines tabs tab-mark))
(setq split-height-threshold 0)
(setq split-width-threshold 0)
(setq helm-split-window-in-side-p t)

(setq-default cursor-type 'bar)
(setq-default indent-tabs-mode nil)

(set-face-font 'default "DejaVu Sans Mono-9")

(setq backup-directory-alist '(("." . "~/.emacs.d/saves")))
(setq flycheck-check-syntax-automatically '(mode-enabled save new-line))

(ac-config-default)
(yas-reload-all)
(helm-projectile-on)
(helm-mode 1)

;; Custom modes.
(projectile-mode)
;(ido-mode)
;(ido-everywhere)
;(flx-ido-mode)
(electric-pair-mode)

;; Disable ido faces to see flx highlights.
(setq ido-enable-flex-matching t)
(setq ido-use-faces nil)

;; Hooks.
(add-hook 'before-save-hook 'whitespace-cleanup)
(add-hook 'prog-mode-hook 'whitespace-mode)
(add-hook 'prog-mode-hook 'linum-mode)
(add-hook 'mu4e-view-mode-hook 'visual-line-mode)
(add-hook 'web-mode-hook 'emmet-mode)
(add-hook 'text-mode-hook 'flyspell-mode)
(add-hook 'prog-mode-hook 'yas-minor-mode)
(add-hook 'org-mode-hook 'org-indent-mode)
(add-hook 'prog-mode-hook 'flycheck-mode)

(add-hook 'dired-mode-hook
 (lambda ()
  (define-key dired-mode-map (kbd "^")
    (lambda () (interactive) (find-alternate-file "..")))
 ))

;; Key bindings.
(global-set-key [C-tab] 'other-window)
(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-x M-g") 'magit-dispatch-popup)
(global-set-key (kbd "C-x w") 'whitespace-mode)
(global-set-key (kbd "C-=") 'er/expand-region)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-x C-b") 'helm-buffers-list)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "M-s o") 'helm-occur)
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)
(global-set-key (kbd "C-c b") 'org-iswitchb)
(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)

;; Linum mode.
(setq linum-format " %3d ")
(set-face-background 'linum (face-background 'default))

;; Slime.
(setq inferior-lisp-program "/usr/bin/sbcl --noinform")
(setq slime-contribs '(slime-fancy))

;; Email.
(setq user-mail-address "feutet@gmail.com")
(setq message-send-mail-function 'sendmail-send-it)
(setq message-kill-buffer-on-exit t)
(setq
 mu4e-maildir       "~/Mail"             ; top-level Maildir
 mu4e-sent-folder   "/[Gmail].Sent Mail" ; folder for sent messages
 mu4e-drafts-folder "/[Gmail].Drafts"    ; unfinished messages
 mu4e-trash-folder  "/[Gmail].Trash"     ; trashed messages
 ;; mu4e-refile-folder "/"               ; saved messages

 mu4e-sent-messages-behavior 'delete
 mu4e-get-mail-command "offlineimap"
 ;; mu4e-update-interval 180
 mu4e-index-cleanup nil               ; don't do a full cleanup check
 mu4e-index-lazy-check t              ; don't consider up-to-date dirs
 mu4e-attachment-dir  "~/Downloads"
 mu4e-compose-dont-reply-to-self t
 mu4e-html2text-command "html2text -utf8 -width 72"
 ;; mail-user-agent 'mu4e-user-agent
 )

;; Web config.
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.jsx?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.s?css\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.vue\\'" . web-mode))
(setq web-mode-content-types-alist
      '(("jsx" . ".*\\.js[x]?\\'")))

(setq
 web-mode-markup-indent-offset 2
 web-mode-css-indent-offset 2
 web-mode-code-indent-offset 2
 web-mode-style-padding 2
 web-mode-script-padding 2
 )

;; wind move: move between windows with shift+arrows.
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))
