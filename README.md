# .emacs.d

Emacs configuration folder.

For now, this just includes the initialisation file `init.el`.

As I'm learning more about Lisp, I'll improve the code and modularise it (if needed).

## Requirements

Since `el-get` fetches and compiles the packages (most of the time), some development packages are needed.

- `make`.
- `gcc`.
- `pkg-config`.
- `libtool`.
- `glib-devel`.
- `gmime-devel`.
- `xapian-core-devel`.
- `texinfo` which provides `makeinfo`.
- `autoconf` which provides `autoreconf`.
- `autoconf-archive`.
- `automake` which provides `aclocal`.

